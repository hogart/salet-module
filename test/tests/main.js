salet.game_id = "2829f25a-c77a-4957-9662-e32b082f9f05";
salet.game_version = "1.0";
salet.autosave = false;
salet.autoload = false;
i18n.push("en", {
  hello: "Hello world!",
  func: function(argument) {
    return "Hello "+argument;
  },
  arg: "Hello {word}!"
})

$(document).ready(function() {
  salet.beginGame();
  QUnit.test("the game is started", function(assert) {
    assert.notEqual(salet, void 0, "Salet is initialized");
    return assert.equal(salet.current, "start", "Salet is in the room called 'start'");
  });
  QUnit.test("Markdown is working", function(assert) {
    return assert.equal(markdown("*hi*"), "<p><em>hi</em></p>\n", "Markdown is good");
  });
  QUnit.test("Translations are good", function(assert) {
    assert.equal("hello".l(), "Hello world!", "Translations were imported fine");
    assert.equal("func".l("world!"), "Hello world!", "Translations can be functions");
    assert.equal("arg".l({word: "world"}), "Hello world!", "Argument substitutions are good");
    assert.equal("choice".l({number: 2}), "Choice 2", "Default translations are good");
    return;
  });
  QUnit.test("RNG is working", function(assert) {
    assert.ok(salet.rnd.randf() >= 0, "Random float is not negative");
    assert.ok(salet.rnd.randf() < 1, "Random float is less than 1");
    assert.equal(salet.rnd.diceString("d1"), 1, "A d1 dice string returns 1");
    assert.equal(salet.rnd.diceString("1d1"), 1, "A 1d1 dice string returns 1");
    assert.equal(salet.rnd.diceString("1d1+1"), 2, "A 1d1+1 dice string returns 2");
    assert.ok(salet.rnd.diceString("d%") > 0, "A d% dice string returns a positive number");
    assert.ok(salet.rnd.diceString("d%") <= 100, "A d% dice string returns a number less than 100");
    assert.ok(salet.rnd.diceString("dF") >= -1, "A Fudge dice string returns a number more or equal to -1");
    return assert.ok(salet.rnd.diceString("dF") <= 1, "A Fudge dice string returns a number less or equal to -1");
  });
  QUnit.test("Inventory", function(assert) {
    var lamp;
    lamp = unit("lamp", {
      display: "lamp description",
      inv: function() {
        return "that's a lamp";
      }
    });
    assert.equal(salet.character.has("lamp"), false, "The character has no lamp");
    salet.character.take(lamp);
    assert.equal(salet.character.has("lamp"), true, "The character has the lamp now");
    assert.equal(salet.character.inv("lamp"), "that's a lamp", "The lamp has an inventory action");
    assert.ok(salet.character.listinv("lamp").match("lamp description"), "The lamp has a description");
    salet.character.drop("lamp");
    assert.equal(salet.character.has("lamp"), false, "The character has no lamp again");
    return;
  });
  QUnit.test("Units in rooms", function(assert) {
    var lamp;
    lamp = unit("lamp", {
      display: "lamp description",
      inv: function() {
        return "that's a lamp";
      }
    });
    salet.rooms["start2"].take(lamp);
    assert.equal(lamp.location, "start2", "The lamp unit is in 'start2' room");
    salet.rooms["start2"].drop("lamp");
    assert.equal(lamp.location, undefined, "The lamp unit is nowhere");
  });
  QUnit.test("Localization", function(assert) {
    assert.ok(window.i18n != null, "The localization is ready");
    window.i18n.push("ru", {
      "hello": "привет"
    });
    return assert.equal(window.i18n.localize("hello", "ru"), "привет", "The localization is working");
  });
  QUnit.test("View", function(assert) {
    salet.view.clearContent();
    assert.equal(jQuery("#content").html(), "", "View clears the content");
    salet.view.append("<p>hello</p>");
    return assert.equal(jQuery("#content").html(), "<p>hello</p>", "View writes new content");
  });
  setTimeout(function() {
    salet.goTo("start2");
    QUnit.test("Salet enters second room", function(assert) {
      assert.equal(salet.current, "start2", "This is the second room");
      return assert.equal($("#content").text(), "This is the second room.\n", "The room description is printed ok.");
    });

    QUnit.test("Player visited the second room", function(assert) {
      assert.equal(salet.isVisited("start2"), true, "Salet assures us the player visited the second room");
      assert.equal(salet.isVisited("third"), false, "Salet assures us the player did not visit the third room yet");
    });

    salet.goTo("start");
    QUnit.test("The second room forbids exiting (boolean)", function(assert) {
      return assert.equal(salet.current, "start2", "This is still the second room");
    });

    salet.goBack()
    QUnit.test("goBack() returns the player back", function(assert) {
      return assert.equal(salet.current, "start2", "The player went back as intended");
    });

    salet.rooms["start2"].canExit = function() {
      return false;
    };
    salet.goTo("start2");
    salet.goTo("start");
    QUnit.test("The second room forbids exiting (function)", function(assert) {
      return assert.equal(salet.current, "start2", "This is still the second room");
    });
    salet.rooms["start2"].canExit = true;

    salet.rooms["start2"].bilink("start");
    QUnit.test("The second room is linked to first one and vice versa", function(assert) {
      assert.equal(salet.rooms["start2"].ways[0], "start", "Second room is linked to the first one");
      assert.equal(salet.rooms["start"].ways[0], "start2", "First room is linked to the second one");
    });
  }, 100);
});

room("start", {
  dsc: ""
});

room("start2", {
  dsc: "This is the second room.",
  canExit: false
});

room("third", {
  dsc: "This is the third room.",
  tags: ["tag", "third"]
});
