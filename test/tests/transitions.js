salet.game_id = "3adc1bc5-2914-431a-8cd1-246a4b89ad7a";
salet.game_version = "1.0";
salet.autosave = false;
salet.autoload = false;

$(document).ready(function() {
  salet.beginGame();
});

room("start", {
  dsc: "This is a regular block.\n\n<div class='transient'>This is a transient block.</div>",
  choices: "#tag",
  tags: ["tag"],
  clear: false,
  optionText: "Next"
});

room("other", {
  tags: ["tag"],
  choices: "#tag",
  clear: false,
  optionText: "Next",
  dsc: "You enter another room. This is a regular block.\n\n<div class='transient'>This is a transient block.</div>",
});
