type TStringLike = string | (() => string);
/** Markdown string */
type TMDString = string;
type TRoomName = string;
type TUnitName = string;
type TRoomTag = string;
type TActionName = string;
type TTimerName = string;
type TLink = TRoomName | TActionName | string;
type EpochTime = number;


interface Random {
    modulo: number;

    randn(): number;
    randf(): number;
    rand(n: number): number;
    randomElement(array: any[]): any;
    randRange(min: number, max: number): number;
    dice(n: number, dx: number, plus: number): number;
    diceString(diceDescription: string): number;
    odds(value: number, target: number, n: number): number;
    seed(n: number): void;
    getSeed(): number;
}


interface SaletProgressSequence {
    link: TLink;
    time: EpochTime;
}

interface SaletProgress {
    seed: number;
    sequence: SaletProgressSequence[];
    path: TRoomName[];
    saveTime: EpochTime;
}


interface SaletRooms {
    [roomName: string]: SaletRoom
}


interface Timer {
    step: number;
    repeatable: boolean;
    action: TActionName
    set: number;
}

interface Timers {
    [timerName: string]: Timer;
}

interface SaletTimers {
    timers: Timers;

    /**
     * Adds a one-time timer `name` that will fire after `steps` clicks with `action`
     * @param name
     * @param action
     * @param [repeat=false]
     * @param [step=1]
     */
    addTimer(name: TTimerName, action: TStringLike, repeat: boolean, step: number): void;

    dropTimer(name: TTimerName): void;

    resetTimer(name: TTimerName): void;

    /** system handler, called every turn */
    checkTimer(): void;
}


interface SaletSaveLoad {
    autosave: boolean;
    autoload: boolean;

    /**
     * If `force` is false, shows prompt if user sure to erase save.
     * Silently erases save otherwise
     * @param [force = false]
     */
    eraseSave(force: boolean): void;

    saveGame(): void;

    loadGame(saveData: object): void;

    getSaveId(): string;

    getSave(): object;
}


interface Salet extends SaletTimers, SaletSaveLoad {
    character: Character;
    game_id: string;
    game_version: string;
    rnd: Random;
    time: EpochTime;
    startTime: EpochTime;
    linkRe: RegExp;
    current: TRoomName;
    interactive: boolean;
    linkStack: TLink[];
    progress: SaletProgress;
    view: SaletView;
    start: TRoomName;
    rooms: SaletRooms;

    init(): void;

    enter(from: TRoomName, to: TRoomName): void;

    afterEnter(from: TRoomName, to: TRoomName): void;

    beforeAction(name: TRoomName, action: TActionName): boolean | void;

    afterAction(name: TRoomName, action: TActionName): boolean | void;

    exit(from: TRoomName, to: TRoomName): void;

    /**
     * Collects rooms by given tag and filters it by size. Gets called for implicit choice autolists
     * @param tag
     * @param length
     */
    getSituationIdChoices(tag: TRoomTag, length: number): SaletRoom[];

    getCurrentRoom(): SaletRoom;

    /** synonym for getCurrentRoom */
    here(): SaletRoom;

    /** Saves click timestamp, adds turn to history */
    processClick(link: TLink): void;

    /** synonym for `processClick` */
    goTo(id: TRoomName): void;

    /**
     * Adds link to execution stack, calls processOneLink, scrolls to new text
     */
    processLink(link: TLink): void;

    /**
     * Executes transition
     */
    processOneLink(link: TLink): void;

    /**
     * Go to room which was visited `n` turns ago
     * n = 1 is current room
     * @param [n=2]
     */
    goBack(n: number): void;

    /**
     *
     * @param id
     * @param [force=false]
     */
    doTransitionTo(id: TRoomName, force: boolean): void;

    getRoomsTagged(tag: TRoomTag): SaletRoom[];

    /**
     * Initializes Salet and starts game. Calls `init` and loads game if there's save and `autoload`
     * is true.
     */
    beginGame(): void;


    isVisited(name: TRoomName): boolean;
}


interface Character {
    inventory: SaletUnit[];

    take(unit: SaletUnit): void;

    drop(unitName: TUnitName): boolean;

    has(unitName: TUnitName): boolean;

    /** Executes `inv` of that unit */
    inv(unitName: TUnitName): void;

    /** Generates unit href */
    listinv(unitName: TUnitName): string;
}


interface SaletUnitProps {
    name: TUnitName;

    order?: number;
    visible?: boolean;
    takeable?: boolean;
    display?: TStringLike;
    take?: TStringLike;
    act?: TStringLike;
    dsc?: TStringLike;
    inv?: TStringLike;
    location?: TRoomName;
}

interface SaletUnit extends SaletUnitProps {
    put(location: TRoomName): void;

    /**
     *
     * @param [location='@location']
     */
    delete(location: TRoomName): void;
}


interface SaletRoomProps {
    name: TRoomName;

    visited?: number;
    title?: string;
    units?: SaletUnit[];
    canView?: boolean;
    canChoose?: boolean;
    priority?: number;
    displayOrder?: number;
    canSave?: boolean;
    ways?: TRoomName[];
    choices?: Array<TRoomName | TRoomTag>;
    maxChoices?: number;
    tags?: TRoomTag[];
    optionText?: TMDString;
    dsc?: TMDString;
    extendSection?: boolean;
    clear?: boolean;
    writers?: string[];
}

interface SaletRoom extends SaletRoomProps {
    canExit(id: TRoomName): boolean;

    exit(to: TRoomName): void;

    enter(from: TRoomName): void;

    /**
     *
     * @param from
     * @param [forceExit=true]
     */
    entering(from: TRoomName, forceExit?: boolean): void;

    take(unit: SaletUnit): void;

    drop(unit: SaletUnit): void;

    act(linkTarget: TLink): void;

    register(): void;

    link(id: TRoomName): void;

    bilink(id: TRoomName): void;
}


interface SaletView {
    init(): void;

    disableSaving(): void;
    enableSaving(): void;

    disableErasing(): void;
    enableErasing(): void;

    enableLoading(): void;
    enableLoading(): void;

    /** Scrolls to `distance` pixels from bottom in 500ms */
    scrollTopTo(distance: number): void;
    /** Scrolls to `distance` pixels from page bottom in 500ms */
    scrollBottomTo(distance: number): void;
    /** Scrolls page bottom in 500ms */
    scrollToBottom(): void;

    /**
     * Clears given block
     * @param [selector='#content']
     */
    clearContent(selector?: string): void;

    /** Transforms argument into text. Calls it if it's function, for jQuery object gets `.html()` */
    prepareContent(content: any): string;

    /** Prepares content and appends it to given element
     * @param content
     * @param [selector='#content']
     */
    append(content: any, selector: string): void;
    /** Synonym for append
     *
     * @param content
     * @param [selector='#content']
     */
    write(content: any, selector: string): void;

    /** Same as append, but replaces element's content
     * @param content
     * @param [selector='#content']
     */
    replace(content: any, selector: string): void;

    /**
     * @param {string} [address] if given, clears all link leading to this
     * @param {string} [selector='#content']
     */
    clearLinks(address?: string, selector?: string): void;

    /**
     * Given a list of room ids, this outputs a standard option block with the room choices in the
     * given order.
     * The contents of each choice will be a link to the room, the text of the link will be given by
     * the room's outputText property. Note that the canChoose function is called, and if it returns
     * false, then the text will appear, but the link will not be clickable.
     *
     * Although canChoose is honored, canView and displayOrder are not. If you need to honor these,
     * you should either do so manually, ot else use the `getSituationIdChoices` method to return
     * an ordered list of valid viewable room ids.
     */
    writeChoices(rooms: TRoomName[]): void;

    /**
     * Shows block with animation (can be redefined). Animation is skipped if Salet is in non-interactive mode
     */
    showBlock(selector: string): void;

    /**
     * Hides block with animation (can be redefined). Animation is skipped if Salet is in non-interactive mode.
     * If `remove` is true, block will be deleted from DOM, otherwise, just hidden
     */
    showBlock(selector: string, remove?: boolean): void;

    /**
     * Returns HTML-string for cycling link
     */
    cycleLink(content: string): string;

    endOutputTransaction(): void;

    hasLocalStorage(): boolean;

    fixClicks(): void;

    mark_all_links_old(): void;

    removeTransient(room?: TRoomName): void;

    updateWays(ways?: TRoomName[], name?: TRoomName): void;
}


interface Window {
    salet: Salet;
    room: (name: TRoomName, props: SaletRoomProps) => SaletRoom;
    unit: (name: TUnitName, props: SaletUnitProps) => SaletUnit;
}
