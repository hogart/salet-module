###
This is the control structure, it has minimal amount of data and
this data is volatile anyway (as in, it won't get saved).

There is only one instance of this class.
###
class Salet
  constructor: (spec) ->
    @character = new Character()

    # REDEFINE THIS IN YOUR GAME
    @game_id = null
    @game_version = "1.0"

    @autosave = true
    @autoload = true

    @rnd = null
    @time = 0

    # Corresponding room names to room objects.
    @rooms = {}

    # The unique id of the starting room.
    @start = "start"

    # Regular expression to catch every link action.
    # Salet's default is a general URL-safe expression.
    @linkRe = /^([0-9A-Za-z_-]+|\.)(\/([0-9A-Za-z_-]+))?$/

    ###
    This function is called at the start of the game. It is
    normally overridden to provide initial character creation
    (setting initial quality values, setting the
    character-text. This is optional, however, as set-up
    processing could also be done by the first room's
    enter function.
    ###
    @init = () ->

    ###
    This function is called before entering any new
    room. It is called before the corresponding room 
    has its `enter` method called.
    ###
    @enter = (oldSituationId, newSituationId) ->

    ###
    Hook for when the room has already been carried out
    and printed.
    ###
    @afterEnter = (oldSituationId, newSituationId) ->

    ###
    This function is called before carrying out any action in
    any room. It is called before the corresponding
    room has its `act` method called.

    If the function returns true, then it is indicating that it
    has consumed the action, and the action will not be passed
    on to the room. Note that this is the only one of
    these global handlers that can consume the event.
    ###
    @beforeAction = (roomId, actionId) ->

    ###
    This function is called after carrying out any action in
    any room. It is called after the corresponding
    room has its `act` method called.
    ###
    @afterAction = (roomId, actionId) ->

    ###
    This function is called after leaving any room. It is
    called after the corresponding room has its `exit`
    method called.
    ###
    @exit = (oldSituationId, newSituationId) ->

    ###
    Returns a list of room ids to choose from, given a set of
    specifications.

    This function is a complex and powerful way of compiling
    implicit room choices. You give it a list of room ids
    and room tags (if a single id or tag is needed just that
    string can be given, it doesn't need to be wrapped in a
    list). Tags should be prefixed with a hash # to differentiate
    them from room ids. The function then considers all
    matching rooms in descending priority order, calling their
    canView functions and filtering out any that should not be
    shown, given the current state. Without additional parameters
    the function returns a list of the room ids at the highest
    level of priority that has any valid results. So, for example,
    if a tag #places matches three rooms, one with priority 2,
    and two with priority 3, and all of them can be viewed in the
    current context, then only the two with priority 3 will be
    returned. This allows you to have high-priority rooms that
    trump any lower situations when they are valid, such as
    situations that force the player to go to one destination if
    the player is out of money, for example.

    If a maxChoices value is given, then the function will not
    return any more than the given number of results. If there are
    more than this number of results possible, then the highest
    priority resuls will be guaranteed to be returned, but the
    lowest priority group will have to fight it out for the
    remaining places.

    Before this function returns its result, it sorts the
    situations in increasing order of their displayOrder values.
    ###
    @getSituationIdChoices = (listOfOrOneIdsOrTags, maxChoices) ->
      datum = null
      i = 0

      # First check if we have a single string for the id or tag.
      if (typeof(listOfOrOneIdsOrTags) == 'string')
        listOfOrOneIdsOrTags = [listOfOrOneIdsOrTags]

      # First we build a list of all candidate ids.
      allIds = []
      for tagOrId in listOfOrOneIdsOrTags
        if (tagOrId.substr(0, 1) == '#')
          ids = @getRoomsTagged(tagOrId.substr(1))
          for id in ids
            allIds.push(id)
        else #it's an id, not a tag
          allIds.push(tagOrId)

      #Filter out anything that can't be viewed right now.
      currentRoom = @getCurrentRoom()
      viewableRoomData = []
      for roomId in allIds
        room = @rooms[roomId]
        assert(room, "unknown_room".l({id:roomId}))

        if (room.canView.fcall(this, currentRoom, room))
          viewableRoomData.push({
            priority: room.priority
            id: roomId
            displayOrder: room.displayOrder
          })

      # Then we sort in descending priority order.
      viewableRoomData.sort((a, b) ->
        return b.priority - a.priority
      )

      committed = []

      # if we need to filter out the results
      if (maxChoices? && viewableRoomData.length > maxChoices)
        viewableRoomData = viewableRoomData[-maxChoices..]

      for candidateRoom in viewableRoomData
        committed.push({
          id: candidateRoom.id
          displayOrder: candidateRoom.displayOrder
        })

      # Now sort in ascending display order.
      committed.sort((a, b) ->
        return a.displayOrder - b.displayOrder
      )

      # And return as a list of ids only.
      result = []
      for i in committed
        result.push(i.id)

      return result

    # This is the data on the player's progress that gets saved.
    @progress = {
      # A random seed string, used internally to make random
      # sequences predictable.
      seed: null
      # Keeps track of the links clicked, and when.
      sequence: [],
      # Keeps track of the rooms visited, for when we want to "go back"
      path: [],
      # The time when the progress was saved.
      saveTime: null
    }

    # The Id of the current room the player is in.
    @current = null

    # Tracks whether we're in interactive mode or batch mode.
    @interactive = true

    # The system time when the game was initialized.
    @startTime = null

    # The stack of links, resulting from the last action, still be to resolved.
    @linkStack = null

    @getCurrentRoom = () ->
      if (@current)
        return @rooms[@current]
      return null

    # Gets the unique id used to identify saved games.
    @getSaveId = () ->
      return 'salet_'+@game_id+'_'+@game_version

    # This gets called when a link needs to be followed, regardless
    # of whether it was user action that initiated it.
    @processLink = (code) ->
      # Check if we should do this now, or if processing is already underway.
      if @linkStack != null
        @linkStack.push(code)
        return

      @view.mark_all_links_old()

      # We're processing, so make the stack available.
      @linkStack = []

      # Handle each link in turn.
      @processOneLink(code)
      while (@linkStack.length > 0)
        code = @linkStack.shift()
        @processOneLink(code)

      # We're done, so remove the stack to prevent future pushes.
      @linkStack = null

      # Scroll to the top of the new content.
      @view.endOutputTransaction()

      # We're able to save, if we weren't already.
      @view.enableSaving()

    @goTo = (roomId) ->
      return @processClick(roomId)

    ###
    This gets called to actually do the work of processing a code.
    When one doLink is called (or a link is clicked), this may set call
    code that further calls doLink, and so on. This method processes
    each one, and processLink manages this.
    ###
    @processOneLink = (code) ->
      match = code.match(@linkRe)
      if not match
        console.error "link_not_valid".l()
        console.error code
        return

      # timer is called before executing an action, but an action can clear
      # the content, so we put the timer's response in a buffer
      timerResponse = @checkTimer()

      situation = match[1]
      action = match[3]

      # Change the situation
      if situation != '.' and situation != @current
        @doTransitionTo(situation)
      else
        # Carry out the action
        if (action)
          room = @getCurrentRoom()
          if room
            consumed = false

            if @beforeAction
              # Try the global act handler
              consumed = @beforeAction(room, action)

            if consumed != true
              room.act(action)
            
            if @afterAction
              @afterAction(room, action)

      @view.append timerResponse

    # This gets called when the user clicks a link to carry out an action.
    @processClick = (code) ->
      now = (new Date()).getTime() * 0.001
      @time = now - @startTime

      # don't save any actions in the room we can't save in
      # and remember that the action of entering the start room is always saved
      match = code.match(@linkRe)
      room = match[1]
      if room
        if room == '.'
          room = @getCurrentRoom()
        else
          room = @getRoom(room)
        assert(room, "unknown_room".l({id:room}))
        if !room.canSave
          # don't save
          return @processLink(code)

      @progress.sequence.push({link:code, when:@time})
      if @getRoom(code)? # if it's a room
        @progress.path.push(code)
      @processLink(code)

    # Go back N rooms. It's not an UNDO.
    # Also, steps = 1 is the current room
    @goBack = (steps = 2) ->
      if @progress.path.length <= steps
        location = @start
      else
        location = @progress.path[@progress.path.length - steps]
      if location?
        @processClick(location)

    # Transition between rooms.
    @doTransitionTo = (newRoomId, force = false) ->
      oldRoomId = @current
      oldRoom = @getCurrentRoom()
      newRoom = @rooms[newRoomId]

      assert(newRoom, "unknown_room".l({id:newRoomId}))

      # We might not have an old situation if this is the start of the game.
      if (oldRoom and @exit)
        @exit(oldRoomId, newRoomId, force)

      @current = newRoomId

      # A global callback before entering
      if (@enter)
        @enter(oldRoomId, newRoomId, force)

      newRoom.entering(oldRoomId, force)

      # additional hook for when the situation text has already been printed
      if (@afterEnter)
        @afterEnter(oldRoomId, newRoomId, force)

    ###
    Erases the character in local storage. This is permanent!
    To restart the game afterwards, we perform a simple page refresh.
    This guarantees authors don't have to care about "tainting" the
    game state across save/erase cycles, meaning that character.sandbox
    no longer has to be the end-all be-all repository of game state.
    ###
    @eraseSave = (force = false) ->
      saveId = @getSaveId() # save slot
      if (localStorage.getItem(saveId) and (force or confirm("erase_message".l())))
        localStorage.removeItem(saveId)
        window.location.reload()

    # Find and return a list of ids for all situations with the given tag.
    @getRoomsTagged = (tag) ->
      result = []
      for id, room of @rooms
        for i in room.tags
          if (i == tag)
            result.push(id)
            break
      return result

    # Saves the character and the walking history to local storage.
    @saveGame = () ->
      # Store when we're saving the game, to avoid exploits where a
      # player loads their file to gain extra time.
      now = (new Date()).getTime() * 0.001
      @progress.saveTime = now - @startTime

      # Save the game.
      localStorage.setItem(@getSaveId(), JSON.stringify({
        progress: @progress
      }))

      # Switch the button highlights.
      @view.disableSaving()
      @view.enableErasing()
      @view.enableLoading()

    # Loads the game from the given data
    @loadGame = (saveFile) ->
      if !@interactive #don't enter recursion!
        return

      @progress = saveFile.progress
      @character = new Character()

      @rnd = new Random(@progress.seed)

      # Start the game now
      @init()

      # Run through all the player's history.
      @interactive = false

      for step in @progress.sequence
        # The action must be done at the recorded time.
        @time = step.when
        @processLink(step.link)

      @interactive = true

      # Reverse engineer the start time.
      now = new Date().getTime() * 0.001
      startTime = now - @progress.saveTime

    @view = new SaletView()

    @getSave = () ->
      id = @getSaveId()
      saveFile = false
      if (@view.hasLocalStorage())
        saveFile = localStorage.getItem(@getSaveId())
        saveFile = JSON.parse(saveFile)
      return saveFile

    @beginGame = () ->
      @view.fixClicks()

      # Handle storage.
      saveFile = @getSave()

      if (saveFile and @autoload)
        try
          @loadGame(saveFile)
          @view.disableSaving()
          @view.enableErasing()
        catch err
          console.log "There was an error loading your save. The save will be overwritten."
          console.error err
      else
        currentDate = new Date()
        @rnd = new Random()
        @progress.seed = @rnd.getSeed()

        @progress.sequence = [{link:@start, when:0}]

        # Start the game
        @startTime = new Date().getTime() * 0.001
        @init()

        # Do the first state.
        @doTransitionTo(@start)

    @getRoom = (name) -> 
      if @rooms[name]?
        return @rooms[name]
      return undefined

    # Just an alias for getCurrentRoom
    @here = () -> @getCurrentRoom()

    @isVisited = (name) ->
      place = @getRoom(name)
      if place
        return Boolean place.visited
      return 0

    @timers = {}
    
    # Adds a one-time timer <name> that will fire after <steps> clicks
    # with <action>.
    # @param name     string              timer ID
    # @param repeat   boolean             should it repeat every <step> steps or not
    # @param action   string or function  code to call
    # @param step     integer             number of steps
    @addTimer = (name, action, repeatable = false, step = 1) ->
      @timers[name] = {
        step: step
        repeatable: repeatable
        action: action
        set: @progress.sequence.length
      }
      return @timers

    @dropTimer = (name) ->
      delete @timers[name]
    
    @resetTimer = (name) ->
      @timers[name].set = @progress.sequence.length

    @checkTimer = () ->
      if Object.keys(@timers).length == 0
        return ""
      response = ""
      for tname, timer of @timers
        if ((@progress.sequence.length - timer.set) == timer.step)
          response += "\n\n"+timer.action.fcall(this)
          if !timer.repeatable
            @dropTimer(tname)
      return response

    for index, value of spec
      this[index] = value

    return this

window.salet = new Salet()
salet.view.init()
