class Character
  constructor: (spec) ->
    @inventory = []

    @take = (thing) =>
      @inventory.push thing
      
    @drop = (thing) =>
      for i in @inventory
        if i.name == thing
          @inventory.remove(i)
          return true
      return false

    @has = (thing) =>
      for i in @inventory
        if i.name == thing
          return true
      return false

    @listinv = (thing) =>
      for i in @inventory
        if i.name == thing
          return invlink(i.display.fcall(i), i.name)

    @inv = (thing) =>
      for i in @inventory
        if i.name == thing
          return i.inv.fcall(i)

    for index, value of spec
      this[index] = value
    return this

character = (spec) ->
  spec ?= {}
  return( new Character(spec) )
