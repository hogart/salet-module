###
fcall() (by analogy with fmap) is added to the prototypes of both String and
Function. When called on a Function, it's an alias for Function#call();
when called on a String, it only returns the string itself, discarding any input.
###

Function.prototype.fcall = Function.prototype.call
Boolean.prototype.fcall = () ->
  return this
String.prototype.fcall = () ->
  return this

assert = (assertion, msg) -> console.assert assertion, msg

way_to = (content, ref) ->
  return "<a href='#{ref}' class='way' id='waylink-#{ref}'>#{content}</a>"

invlink = (content, ref) ->
  return "<a href='./_inv_#{ref}' class='once'>#{content.fcall()}</a>"

Array::remove = (e) -> @[t..t] = [] if (t = @indexOf(e)) > -1

addClass = (element, className) ->
  if (element.classList)
    element.classList.add(className)
  else
    element.className += ' ' + className

unitlink = (content, ref) ->
  return "<a href='./_act_#{ref}' class='once'>#{content}</a>"

parsedsc = (text, name) ->
  window.unitname = name
  text = text.replace /\{\{(.+)\}\}/g, (str, p1) ->
    name = window.unitname
    window.unitname = undefined
    return unitlink(p1, name)
  return text
