# Internationalization support
class Localize
  constructor: (lang = "en") ->
    @lang = document.getElementsByTagName("html")[0].getAttribute("lang") || lang

  # Default Messages
  strings:
    en:
      choice: "Choice {number}",
      link_not_valid: "The link '{link}' doesn't appear to be valid.",
      link_no_action: "A link with a situation of '.', must have an action.",
      unknown_room: "Room not found: {id}.",
      erase_message: "This will permanently delete this character and immediately return you to the start of the game. Are you sure?",
      dice_string_error: "Couldn't interpret your dice string: '{string}'."

  push: (lang, strings) ->
    if @strings[lang]?
      @strings[lang] = $.extend @strings[lang], strings
    else
      @strings[lang] = strings

  localize: (message, languageCode = @lang) ->
    if @strings[languageCode]?
      localized = @strings[languageCode][message]
      if localized
        return localized
    return message

window.i18n = new Localize()

# API
String.prototype.l = (args) ->
  # Find the localized form.
  localized = window.i18n.localize(this)
  if typeof(localized) == "function"
    localized = localized(args)
  else # Merge in any replacement content.
    if args
      for name of args
        localized = localized.replace(
          new RegExp("\\{"+name+"\\}"), args[name]
        )
  return localized
