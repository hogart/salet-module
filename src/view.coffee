###
Salet interface configuration.
In a typical MVC structure, this is the View.
Only it knows about the DOM structure.
Other modules just use its API or prepare the HTML for insertion.
You don't need to call this module from the game directly.

The abstraction goal here is to provide the author with a freedom to style his
game as he wants to. The save and erase buttons are not necessary buttons,
but they could be something else entirely. (That's why IDs are hardcoded.)

There is only one instance of this class, and it's stored as `salet.view`.
###

class SaletView
  constructor: (spec) ->
    @init = () ->
      $("#page").on("click", "a", (event) ->
        event.preventDefault()
        a = $(this)
        href = a.attr('href')
        if a.hasClass("once") || href.match(/[?&]once[=&]?/)
          salet.view.clearLinks(href)
        if href.match(salet.linkRe)
          salet.processClick(href)
      )
      $("#load").on("click", "a", (event) ->
        window.location.reload()
      )
      if (@hasLocalStorage())
        $(document).on("click", "#erase", (event) ->
          event.preventDefault()
          return salet.eraseSave()
        )
        $(document).on("click", "#save", (event) ->
          event.preventDefault()
          return salet.saveGame()
        )
    @disableSaving = () ->
      $("#save").addClass('disabled')
    @enableSaving = () ->
      $("#save").removeClass('disabled')
    @enableErasing = () ->
      $("#erase").removeClass('disabled')
    @disableErasing = () ->
      $("#erase").addClass('disabled')
    @enableLoading = () ->
      $("#load").removeClass('disabled')
    @disableLoading = () ->
      $("#load").addClass('disabled')

    # Scrolls the top of the screen to the specified point
    @scrollTopTo = (value) ->
      $('html,body').stop().animate({scrollTop: value}, 500)

    # Scrolls the bottom of the screen to the specified point
    @scrollBottomTo = (value) ->
      @scrollTopTo(value - $(window).height())

    # Scrolls all the way to the bottom of the screen
    @scrollToBottom = () ->
      @scrollTopTo($('html').height() - $(window).height());

    ###
    Removes all content from the page, clearing the main content area.

    If an elementSelector is given, then only that selector will be
    cleared. Note that all content from the cleared element is removed,
    but the element itself remains, ready to be filled again using @write.
    ###
    @clearContent = (elementSelector = "#content") ->
      if (elementSelector == "#content") # empty the intro with the content
        intro = document.getElementById("intro")
        if intro?
          intro.innerHTML = ""
      document.querySelector(elementSelector).innerHTML = ""

    @prepareContent = (content) ->
      if typeof content == "function"
        content = content()
      if content instanceof $
        content = content[0].outerHTML
      return content.toString()

    # Write content to current room
    @write = (content, elementSelector = "#current-room") =>
      @append(content, elementSelector)

    # Append content to a block. Does not replace the old content.
    @append = (content, elementSelector = "#content") ->
      if not content? or content == ""
        return
      content = @prepareContent(content)
      block = $(elementSelector)
      if block
        newhtml = markdown(content)
        block.promise().done(() ->
          block.append(newhtml)
        )
        return newhtml
      else
        # most likely this is the starting room
        block = $("#content")
        block.html(content)
        return block.html()

    # Replaces the text in the given block with the given text.
    # !! Does not call markdown on the provided text. !!
    @replace = (content, elementSelector) ->
      if content == ""
        return
      content = @prepareContent(content)
      block = $(elementSelector)
      block.promise().done(() ->
        block.html(content)
      )

    ###
    Turns any links that target the given href into plain
    text. This can be used to remove action options when an action
    is no longer available. It is used automatically when you give
    a link the 'once' class.

    @param code remove all links to given target (optional)
    @param container remove all links inside given block (optional)
    ###
    @clearLinks = (code, container) ->
      if code
        unless container?
          container = "#page"
        links = $(container).find("a[href='" + code + "']")
      else
        unless container?
          container = "#content"
        links = $(container).find("a")
      for a in links
        a = $(a)
        if not code and (a.hasClass('sticky') || a.attr("href").match(/[?&]sticky[=&]?/))
          return
        a.replaceWith($("<span>").addClass("ex_link").html(a.html()))
      return true

    ###
    Given a list of room ids, this outputs a standard option
    block with the room choices in the given order.

    The contents of each choice will be a link to the room,
    the text of the link will be given by the room's
    outputText property. Note that the canChoose function is
    called, and if it returns false, then the text will appear, but
    the link will not be clickable.

    Although canChoose is honored, canView and displayOrder are
    not. If you need to honor these, you should either do so
    manually, ot else use the `getSituationIdChoices` method to
    return an ordered list of valid viewable room ids.
    ###
    @writeChoices = (listOfIds) ->
      if (not listOfIds? or listOfIds.length == 0)
        return

      currentRoom = salet.getCurrentRoom()
      $options = $("<ul>").addClass("options")
      for roomId, i in listOfIds
        room = salet.rooms[roomId]
        assert(room, "unknown_situation".l({id:roomId}))
        if (room == currentRoom)
          continue

        optionText = room.optionText.fcall(room, currentRoom.name)
        if (!optionText)
          optionText = "choice".l({number:i+1})
        $option = $("<li>")
        $a = $("<span>")
        if (room.canChoose.fcall(this, currentRoom))
          $a = $("<a>").attr({href: roomId})
        $a.html(optionText)
        $option.html($a)
        $options.append($option)
      @write($options)

    # Marks all links as old. This gets called in a `processLink` function.
    @mark_all_links_old = () ->
      $('#page .new').removeClass('new')

    # Removes links and transient sections.
    @removeTransient = (room = undefined) =>
      container = "#content"
      if (room)
        container = ".room-#{room}"

      transientFilter = () ->
        return $(this).attr("href").match(/[?&]transient[=&]?/)
      contentToHide = $("#{container} .transient")
      contentToHide = contentToHide.add("#{container} .options")
      contentToHide = contentToHide.add(
        $("#{container} a").filter(transientFilter)
      )
      @hideBlock(contentToHide, true)
      @clearLinks(undefined, container)

    # At last, we scroll the view so that .new objects are in view.
    @endOutputTransaction = () ->
      if !salet.interactive
        return # We're loading a save; do nothing at all.
      $new = $('.new')
      if $new.length == 0
        return # Somehow, there's nothing new.
      viewHeight = $(window).height()
      optionHeight = 0
      newTop = $new.first().offset().top
      newBottom = $new.last().offset().top + $new.last().height()
      newHeight = newBottom - newTop

      # We take the options list into account, because we don't want the new
      # content to scroll offscreen when the list disappears. So we calculate
      # scroll points as though the option list was already gone.
      if ($('.options').not('.new').length)
        optionHeight = $('.options').not('new').height()

      if (newHeight > (viewHeight - optionHeight - 50))
        # The new content is too long for our viewport, so we scroll the
        # top of the new content to roughly 75% of the way up the viewport's
        # height.
        return @scrollTopTo(newTop-(viewHeight*0.25) - optionHeight)
      
      if (newTop > $('body').height() - viewHeight)
        # If we scroll right to the bottom, the new content will be in
        # view. So we do that.
        return @scrollToBottom()
      
      # Our new content is too far up the page. So we scroll to place
      # it somewhere near the bottom.
      return @scrollBottomTo(newBottom+100 - optionHeight)

    # Feature detection
    @hasLocalStorage = () ->
      return window.localStorage?

    # Any point that an option list appears, its options are its first links.
    @fixClicks = () ->
      $("body").on('click', "ul.options li", (event) ->
        # Make option clicks pass through to their first link.
        link = $("a", this)
        if (link.length > 0)
          $(link.get(0)).click()
      )

    @showBlock = (block) ->
      if salet.interactive
        $(block).slideDown('slow', () ->
          $(block).fadeTo(500, 1)
        )
      else
        $(block).show()

    # A function to hide a block
    # @param block - jQuery object or selector to hide
    # @param remove - whether to remove the block from DOM afterwards
    @hideBlock = (block, remove = false) ->
      block = $(block)
      if not salet.interactive
        if remove
          return block.remove()
        else
          return block.hide()

      callback = () ->
        $(this).remove()
      if remove == false
        callback = undefined
      block.finish().fadeTo(500, 0, () ->
        block.slideUp(250, callback)
      )
      
      return
    @updateWays = (ways, name) ->
      if document.getElementById("ways") == null
        return
      content = ""
      if ways then for way in ways
        if salet.rooms[way]?
          title = salet.rooms[way].title.fcall(this, name)
          content += "<li class='nav-item'><a class='nav-link' href='#{way}'>#{title}</a></li>"
          @replace(content, "#ways")
          @showBlock(".ways #ways_hint")
        else
          @hideBlock(".ways #ways_hint", false)
    @cycleLink = (content) ->
      return "<a href='./_replacer_cyclewriter' class='cycle' id='cyclewriter'>#{content}</a>"
    
    for index, value of spec
      this[index] = value

    return this
