# A unit class.
# A unit cannot be in several locations at once, you must clone the variable.
class SaletUnit
  constructor: (spec) ->
    unless spec.name?
      console.error("Trying to create a unit with no name")
      return null

    @order = 0 # you can use this to sort the descriptions
    @visible = true
    @look = (f) =>
      if @dsc and @dsc != "" and @visible
        text = @dsc.fcall(this, f).toString()
        # replace braces {{}} with link to _act_
        return parsedsc(text, @name)
    @takeable = false
    @display = ""
    @take = () => "You take the #{@display.fcall(@)}." # taking to inventory
    @act = () => "You don't find anything extraordinary about the #{@display.fcall(@)}." # unit action
    @dsc = () => "You see a {{#{@display.fcall(@)}}} here." # unit description
    @inv = () => "It's a #{@display.fcall(@)}." # inventory description
    @location = ""
    @put = (location) =>
      if salet.rooms[location]?
        salet.rooms[location].take(this)
      else
        console.error("Could not find location #{location} for a unit #{@name}")
    @delete = (location = false) =>
      if location == false
        location = @location
      salet.rooms[location].drop(this)

    for key, value of spec
      this[key] = value
  
window.unit = (name, spec) ->
  spec ?= {}
  spec.name = name
  return new SaletUnit(spec)
