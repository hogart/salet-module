class SaletRoom
  constructor: (spec) ->
    @visited = 0

    # unique room ID, mandatory to edit
    @name = "Room"
    # room title, used in waypoint generation, see @ways
    @title = "Room"

    @units = []
    @canView = true
    @canChoose = true
    @priority = 1
    @displayOrder = 1
    @canSave = true
    @canExit = true
    @tags = []
    # array of adjacent room IDs, printed as links. Link text to this room is @title.
    @ways = []
    @choices = ""
    @optionText = "Choice"

    @dsc = false # room description
    @extendSection = false
    @clear = true # clear the screen on entering the room?
    ###
      I call SaletRoom.exit every time the player exits to another room.
      Unlike @after this gets called after the section is closed.
      It's a styling difference.
    ###
    @exit = (to) =>
      return true

    ###
      I call SaletRoom.enter every time the player enters this room but before the section is opened.
      Unlike @before this gets called before the current section is opened.
      It's a styling difference.

      The upstream Undum version does not allow you to redefine @enter function easily but allows custom @exit one.
      It was renamed as @entering to achieve API consistency.
    ###
    @enter = (from) =>
      return true

    ###
      Salet calls SaletRoom.entering every time a situation is entered, and
      passes it a string referencing the previous situation, or null if there is
      none (ie, for the starting situation).

      If f == this.name (we're in the same location) the `before` and `after` callbacks are ignored.
    ###
    @entering = (f, force = false) =>
      if (
        f != @name and
        salet.rooms[f]? and
        salet.rooms[f].canExit? and
        (
          salet.rooms[f].canExit == false or
          salet.rooms[f].canExit.fcall(salet.rooms[f], @name) == false
        ) and
        force == false
      )
        # we need to return even if the current room forbids exiting
        return salet.doTransitionTo(f, true)

      if @clear and f?
        salet.view.clearContent()

      if salet.rooms[f]? and not @clear
        if salet.rooms[f].extendSection == false
          salet.view.removeTransient(f)
        else
          salet.view.removeTransient()

      if f != @name and salet.rooms[f]?
        @visited++
        if salet.rooms[f].exit?
          salet.rooms[f].exit @name

      if @enter
        @enter f

      if not @extendSection
        classes = if @classes then ' ' + @classes.join(' ') else ''
        room = document.getElementById('current-room')
        if room?
          room.removeAttribute('id')
        salet.view.append "<section id='current-room' data-room='#{@name}' class='room-#{@name}#{classes}'></section>"

      if f != @name and @before?
        salet.view.write markdown(@before.fcall(this, f))

      salet.view.write @look f

      if f != @name and @after?
        salet.view.write markdown(@after.fcall(this, f))

      if @beforeChoices?
        @beforeChoices.fcall(this, f)

      if @choices
        salet.view.writeChoices(salet.getSituationIdChoices(@choices, @maxChoices))

      if @afterChoices?
        @afterChoices.fcall(this, f)

      if salet.autosave and @canSave
        salet.saveGame()

    # A string between unit descriptions.
    @unitDelimiter = ""

    ###
      An internal function to get the room's description and the descriptions of
      every unit in this room.
    ###
    @look = (f) =>
      salet.view.updateWays(@ways, @name)
      retval = ""

      # Print the room description
      if @dsc and @dsc != ""
        dsc = @dsc.fcall(this, f).toString()
        retval += markdown(dsc)

      unitDescriptions = []
      for thing in @units
        if thing.name and typeof(thing.look) == "function" and thing.look(f)
          unitDescriptions.push ({
            order: thing.order,
            content: thing.look(f)
          })

      unitDescriptions.sort((a, b) ->
        return a.order - b.order
      )

      for description in unitDescriptions
        retval += @unitDelimiter + description.content

      return markdown(retval)

    ###
      Places a unit in this room.
    ###
    @take = (thing) =>
      thing.location = @name
      @units.push(thing)

    @drop = (name) =>
      for thing in @units
        if thing.name == name
          @units.splice(@units.indexOf(thing), 1)
          thing.location = null
          return @units

    ###
      Unit action. A function or a string which comes when you click on a link in unit description.
      You could interpret this as an EXAMINE verb or USE one, it's your call.
    ###
    @act = (action) =>
      if (link = action.match(/^_(act|cycle|inv)_(.+)$/)) #unit action
        if link[1] == "inv"
          return salet.view.write salet.character.inv(link[2])
        for thing in @units
          if thing.name == link[2]
            if link[1] == "act"
              # If it's takeable, the player can take this unit.
              # If not, we check the "act" function.
              if thing.takeable
                salet.character.take(thing)
                @drop link[2]
                return salet.view.write(thing.take.fcall(thing).toString())
              else if thing.act?
                return salet.view.write thing.act.fcall(thing)

        # the loop is done but no return came - match not found
        console.error("Could not find #{link[2]} in current room.")

      # we're done with units, now check the regular actions
      actionClass = action.match(/^_(\w+)_(.+)$/)
      that = this

      responses = {
        writer: (ref) ->
          content = that.writers[ref].fcall(that, action)
          output = markdown(content)
          salet.view.write(output)
        replacer: (ref) ->
          content = that.writers[ref].fcall(that, action)
          salet.view.replace(content, '#'+ref)
        inserter: (ref) ->
          content = that.writers[ref].fcall(that, action)
          output = markdown(content)
          salet.view.write(output, '#'+ref)
      }

      if (actionClass)
        # Matched a special action class
        [responder, ref] = [actionClass[1], actionClass[2]]

        if(!@writers.hasOwnProperty(actionClass[2]))
          throw new Error("Tried to call undefined writer: #{action}");
        responses[responder](ref);
      else if (@actions.hasOwnProperty(action))
        @actions[action].call(this, action);
      else
        throw new Error("Tried to call undefined action: #{action}");

    @register = () =>
      if not @name?
        console.error("Room has no name")
        return this
      salet.rooms[@name] = this
      return this

    @writers = {}

    # Short way of saying "this room is connected to that room"
    @link = (name) =>
      if salet.rooms[name]
        @ways.push(name)

    # Short way of saying "this room is connected to that room and vice versa"
    @bilink = (name) =>
      another = salet.rooms[name]
      if another
        @link(name)
        another.link(@name)

    for index, value of spec
      this[index] = value
    return this

window.room = (name, spec) ->
  spec ?= {}
  spec.name = name
  return new SaletRoom(spec).register()
